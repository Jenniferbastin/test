<?php
/* Smarty version 3.1.43, created on 2022-12-11 14:33:51
  from 'C:\wamp64\www\Prestashop\modules\smartsupp\views\templates\front\chat_widget.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_6395dc3fd2fda9_32323871',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9154b0fac82b23c5e5e595ed466287ecc93b6e79' => 
    array (
      0 => 'C:\\wamp64\\www\\Prestashop\\modules\\smartsupp\\views\\templates\\front\\chat_widget.tpl',
      1 => 1670446178,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6395dc3fd2fda9_32323871 (Smarty_Internal_Template $_smarty_tpl) {
echo '<?php
';?>
/**
 * NOTICE OF LICENSE
 *
 * Smartsupp live chat - official plugin. Smartsupp is free live chat with visitor recording. 
 * The plugin enables you to create a free account or sign in with existing one. Pre-integrated 
 * customer info with WooCommerce (you will see names and emails of signed in webshop visitors).
 * Optional API for advanced chat box modifications.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Smartsupp
 *  @copyright 2021 Smartsupp.com
 *  @license   GPL-2.0+
**/ 
<?php echo '?>';?>


<?php echo $_smarty_tpl->tpl_vars['smartsupp_js']->value;
}
}
