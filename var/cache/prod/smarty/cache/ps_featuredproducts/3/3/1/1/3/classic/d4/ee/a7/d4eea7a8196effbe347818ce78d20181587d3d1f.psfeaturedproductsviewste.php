<?php
/* Smarty version 3.1.43, created on 2022-12-11 15:18:37
  from 'module:psfeaturedproductsviewste' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_6395e6bd582ff5_35585025',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:psfeaturedproductsviewste',
      1 => 1670234310,
      2 => 'module',
    ),
    'c5fe78c55e85c95282e9cb0a2fc49ce692d31100' => 
    array (
      0 => 'C:\\wamp64\\www\\Prestashop\\themes\\classic\\templates\\catalog\\_partials\\productlist.tpl',
      1 => 1670234309,
      2 => 'file',
    ),
    '59d2a6ef2a0e7e73fd6385b533c4f150cf65106a' => 
    array (
      0 => 'C:\\wamp64\\www\\Prestashop\\themes\\classic\\templates\\catalog\\_partials\\miniatures\\product.tpl',
      1 => 1670234309,
      2 => 'file',
    ),
    'ac87e9697536730f9e95e5567e2d421a6c29c544' => 
    array (
      0 => 'C:\\wamp64\\www\\Prestashop\\themes\\classic\\templates\\catalog\\_partials\\product-flags.tpl',
      1 => 1670234309,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_6395e6bd582ff5_35585025 (Smarty_Internal_Template $_smarty_tpl) {
?><section class="featured-products clearfix">
  <h2 class="h2 products-section-title text-uppercase">
    Produits populaires
  </h2>
  

<div class="products row">
            
<div class="js-product product col-xs-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="26" data-id-product-attribute="0">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="http://localhost/prestashop/fr/accueil/26-sapin-en-bois.html" class="thumbnail product-thumbnail">
              <img
                src="http://localhost/prestashop/1-home_default/sapin-en-bois.jpg"
                alt="Sapin en bois 60 cm"
                loading="lazy"
                data-full-size-image-url="http://localhost/prestashop/1-large_default/sapin-en-bois.jpg"
                width="250"
                height="250"
              />
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/26-sapin-en-bois.html" content="http://localhost/prestashop/fr/accueil/26-sapin-en-bois.html">Sapin en bois 60 cm</a></h3>
                  

        
                      <div class="product-price-and-shipping">
                              

                <span class="regular-price" aria-label="Prix de base">36,30 €</span>
                                  <span class="discount-amount discount-product">-7,26 €</span>
                              
              

              <span class="price" aria-label="Prix">
                                                  29,04 €
                              </span>

              

              
            </div>
                  

        
          
        
      </div>

      
    <ul class="product-flags js-product-flags">
                    <li class="product-flag on-sale">Promo !</li>
                    <li class="product-flag discount">-7,26 €</li>
                    <li class="product-flag new">Nouveau</li>
            </ul>

    </div>
  </article>
</div>

            
<div class="js-product product col-xs-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="27" data-id-product-attribute="0">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="http://localhost/prestashop/fr/accueil/27-sapin-de-noel-ecologique.html" class="thumbnail product-thumbnail">
              <img
                src="http://localhost/prestashop/2-home_default/sapin-de-noel-ecologique.jpg"
                alt="Sapin de Noël naturel 1M"
                loading="lazy"
                data-full-size-image-url="http://localhost/prestashop/2-large_default/sapin-de-noel-ecologique.jpg"
                width="250"
                height="250"
              />
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/27-sapin-de-noel-ecologique.html" content="http://localhost/prestashop/fr/accueil/27-sapin-de-noel-ecologique.html">Sapin de Noël naturel 1M</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Prix">
                                                  72,60 €
                              </span>

              

              
            </div>
                  

        
          
        
      </div>

      
    <ul class="product-flags js-product-flags">
                    <li class="product-flag new">Nouveau</li>
            </ul>

    </div>
  </article>
</div>

            
<div class="js-product product col-xs-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="28" data-id-product-attribute="0">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="http://localhost/prestashop/fr/accueil/28-boule-de-noel-rouge.html" class="thumbnail product-thumbnail">
              <img
                src="http://localhost/prestashop/4-home_default/boule-de-noel-rouge.jpg"
                alt="Boule de Noël rouge"
                loading="lazy"
                data-full-size-image-url="http://localhost/prestashop/4-large_default/boule-de-noel-rouge.jpg"
                width="250"
                height="250"
              />
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/28-boule-de-noel-rouge.html" content="http://localhost/prestashop/fr/accueil/28-boule-de-noel-rouge.html">Boule de Noël rouge</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Prix">
                                                  4,84 €
                              </span>

              

              
            </div>
                  

        
          
        
      </div>

      
    <ul class="product-flags js-product-flags">
                    <li class="product-flag new">Nouveau</li>
            </ul>

    </div>
  </article>
</div>

            
<div class="js-product product col-xs-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="29" data-id-product-attribute="0">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="http://localhost/prestashop/fr/accueil/29-couronne-de-noel.html" class="thumbnail product-thumbnail">
              <img
                src="http://localhost/prestashop/5-home_default/couronne-de-noel.jpg"
                alt="Couronne de Noël"
                loading="lazy"
                data-full-size-image-url="http://localhost/prestashop/5-large_default/couronne-de-noel.jpg"
                width="250"
                height="250"
              />
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/29-couronne-de-noel.html" content="http://localhost/prestashop/fr/accueil/29-couronne-de-noel.html">Couronne de Noël</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Prix">
                                                  21,78 €
                              </span>

              

              
            </div>
                  

        
          
        
      </div>

      
    <ul class="product-flags js-product-flags">
                    <li class="product-flag new">Nouveau</li>
            </ul>

    </div>
  </article>
</div>

            
<div class="js-product product col-xs-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="30" data-id-product-attribute="0">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="http://localhost/prestashop/fr/accueil/30-guirlande-lumineuse-2m.html" class="thumbnail product-thumbnail">
              <img
                src="http://localhost/prestashop/6-home_default/guirlande-lumineuse-2m.jpg"
                alt="Guirlande lumineuse 2M"
                loading="lazy"
                data-full-size-image-url="http://localhost/prestashop/6-large_default/guirlande-lumineuse-2m.jpg"
                width="250"
                height="250"
              />
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/30-guirlande-lumineuse-2m.html" content="http://localhost/prestashop/fr/accueil/30-guirlande-lumineuse-2m.html">Guirlande lumineuse 2M</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Prix">
                                                  24,20 €
                              </span>

              

              
            </div>
                  

        
          
        
      </div>

      
    <ul class="product-flags js-product-flags">
                    <li class="product-flag new">Nouveau</li>
            </ul>

    </div>
  </article>
</div>

    </div>
  <a class="all-product-link float-xs-left float-md-right h4" href="http://localhost/prestashop/fr/2-accueil">
    Tous les produits<i class="material-icons">&#xE315;</i>
  </a>
</section>
<?php }
}
