<?php
/* Smarty version 3.1.43, created on 2022-12-11 14:33:52
  from 'module:pslinklistviewstemplatesh' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_6395dc40695d79_20484391',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '906548e89c8c6025457ddaeffb1980a0c743b872' => 
    array (
      0 => 'module:pslinklistviewstemplatesh',
      1 => 1670234309,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_6395dc40695d79_20484391 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => 'C:\\wamp64\\www\\Prestashop\\var\\cache\\prod\\smarty\\compile\\classiclayouts_layout_full_width_tpl\\82\\b9\\fa\\82b9fa5f76146969137d771a21cf3d1de368086a_2.file.helpers.tpl.php',
    'uid' => '82b9fa5f76146969137d771a21cf3d1de368086a',
    'call_name' => 'smarty_template_function_renderLogo_9419877056395dc401e8ee3_34433850',
  ),
));
?><div class="col-md-6 links">
  <div class="row">
      <div class="col-md-6 wrapper">
      <p class="h3 hidden-sm-down">Produits</p>
      <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_1" data-toggle="collapse">
        <span class="h3">Produits</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_1" class="collapse">
                  <li>
            <a
                id="link-product-page-prices-drop-1"
                class="cms-page-link"
                href="http://localhost/prestashop/fr/promotions"
                title="Our special products"
                            >
              Promotions
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-new-products-1"
                class="cms-page-link"
                href="http://localhost/prestashop/fr/nouveaux-produits"
                title="Nos nouveaux produits"
                            >
              Nouveaux produits
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-best-sales-1"
                class="cms-page-link"
                href="http://localhost/prestashop/fr/meilleures-ventes"
                title="Nos meilleures ventes"
                            >
              Meilleures ventes
            </a>
          </li>
              </ul>
    </div>
      <div class="col-md-6 wrapper">
      <p class="h3 hidden-sm-down">Notre société</p>
      <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_2" data-toggle="collapse">
        <span class="h3">Notre société</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_2" class="collapse">
                  <li>
            <a
                id="link-static-page-contact-2"
                class="cms-page-link"
                href="http://localhost/prestashop/fr/nous-contacter"
                title="Utiliser le formulaire pour nous contacter"
                            >
              Contactez-nous
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-sitemap-2"
                class="cms-page-link"
                href="http://localhost/prestashop/fr/plan-site"
                title="Vous êtes perdu ? Trouvez ce que vous cherchez"
                            >
              Plan du site
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-stores-2"
                class="cms-page-link"
                href="http://localhost/prestashop/fr/magasins"
                title=""
                            >
              Magasins
            </a>
          </li>
              </ul>
    </div>
    </div>
</div>
<?php }
}
