<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerVcxcfpb\appProdProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerVcxcfpb/appProdProjectContainer.php') {
    touch(__DIR__.'/ContainerVcxcfpb.legacy');

    return;
}

if (!\class_exists(appProdProjectContainer::class, false)) {
    \class_alias(\ContainerVcxcfpb\appProdProjectContainer::class, appProdProjectContainer::class, false);
}

return new \ContainerVcxcfpb\appProdProjectContainer([
    'container.build_hash' => 'Vcxcfpb',
    'container.build_id' => '20fe6227',
    'container.build_time' => 1670754209,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerVcxcfpb');
